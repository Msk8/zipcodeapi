# ZipcodeAPI

This is a simple API used in conjunction with ZipcodeFrontend, to show and search for zipcodes in Nicaragua

## Languages
This API was made in Node.js 10.16 and Sqlite3 5.0.2 , so it's required to install it in order to use it.


## Installation

Use the command prompt to install ZipcodeAPI dependencies.

```bash
npm install
```

## Usage

In the command prompt, run:

```
npm run dev
```

## Endpoints

This project contains 2 get endpoints:

```
http://localhost:4000/api/zipcodes
```

This returns every zipcode with all its information.

```
http://localhost:4000/api/zipcodes/:postalCode
```

This one returns  the information of only 1 postal code, the parameter sent is the postal code itself. For example: http://localhost:4000/api/zipcodes/10000