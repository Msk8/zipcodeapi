const express = require("express");
const app = express();
const PORT = process.env.PORT || 4000;
const db = require("./config/db");
const cors = require("cors");

app.use(express.json());
app.use(cors());

app.get("/", (req, res, next) => {
    res.json({"message":"Ok"})
});

app.get("/api/zipcodes", (req, res, next) => {
    var sql = "select * from zipcodes order by postalCode"
    var params = []
    db.all(sql, params, (err, rows) => {
        if (err) {
          res.status(400).json({"error":err.message});
          return;
        }
        res.json({
            "message":"success",
            "data":rows
        })
      });
});

app.get("/api/zipcodes/:id", (req, res, next) => {
    var sql = "select * from zipcodes where postalCode = ? "
    var params = [req.params.id]
    db.get(sql, params, (err, row) => {
        if (err) {
          res.status(400).json({"error":err.message});
          return;
        }

        if(row === undefined){
        	res.status(400).json({"error": "No zipcode found"});
        	return;
        }else{
        	res.json({
            	"message":"success",
            	"data":row
        	})	
        }
        
      });
});

app.use(function(req, res){
    res.status(404);
});

app.listen(PORT, () => {
	console.log(`app started on port ${PORT}`)
});

